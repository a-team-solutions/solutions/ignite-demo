package grid.client.flow;

import java.util.Date;

import javax.cache.Cache;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.ContinuousQuery;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.transactions.Transaction;

import grid.Caches;
import grid.model.OrderContext;
import grid.server.Conf;

public class PayCheck {

    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);
        Ignite ignite = Ignition.start(new Conf());

        // Activate the cluster. Required to do if the persistent store is enabled because you might need
        // to wait while all the nodes, that store a subset of data on disk, join the cluster.
        ignite.cluster().active(true);

        IgniteCache<String, OrderContext> orderCache = ignite.getOrCreateCache(Caches.OREDR);

        ContinuousQuery<String, OrderContext> cqry = new ContinuousQuery<>();

        IgniteTransactions transactions = ignite.transactions();

        cqry.setLocalListener(evts ->
            evts.forEach(e -> {
                OrderContext o = e.getValue();
                orderFlagPayed(orderCache, transactions, o);
                System.out.println("payed >>" + e.getEventType() + " " + o);
            }));

        cqry.setRemoteFilterFactory(() -> e -> e.getValue().getPayDate() == null);

        cqry.setInitialQuery(new ScanQuery<String, OrderContext>(
            (k, o) -> o.getPayDate() == null));

        QueryCursor<Cache.Entry<String, OrderContext>> cur = orderCache.query(cqry);
        for (Cache.Entry<String, OrderContext> e : cur) {
            OrderContext o = e.getValue();
            orderFlagPayed(orderCache, transactions, o);
            System.out.println("payed > " + e.getValue());
        }
    }

    private static void orderFlagPayed(
            IgniteCache<String, OrderContext> orderCache,
            IgniteTransactions transactions,
            OrderContext o) {
        try (Transaction tx = transactions.txStart()) {
            o.setPayDate(new Date());
            orderCache.put(o.getId(), o);
            tx.commit();
        }
    }
}
