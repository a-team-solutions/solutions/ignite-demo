package grid.client.flow;

import javax.cache.Cache;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.ContinuousQuery;
import org.apache.ignite.cache.query.QueryCursor;

import grid.Caches;
import grid.model.OrderContext;
import grid.server.Conf;

public class Changes {

    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);
        Ignite ignite = Ignition.start(new Conf());

        // Activate the cluster. Required to do if the persistent store is enabled because you might need
        // to wait while all the nodes, that store a subset of data on disk, join the cluster.
        ignite.cluster().active(true);

        IgniteCache<String, OrderContext> orderCache = ignite.getOrCreateCache(Caches.OREDR);

        ContinuousQuery<String, OrderContext> cqry = new ContinuousQuery<>();

        cqry.setLocalListener(evts ->
            evts.forEach(e -> {
                // if (e.getEventType() == EventType.UPDATED) {
                    System.out.println("change: " + e);
                // }
            }));

        cqry.setRemoteFilterFactory(() -> e -> true);

        // cqry.setInitialQuery(new ScanQuery<String, OrderContext>((k, o) -> true));

        QueryCursor<Cache.Entry<String, OrderContext>> cur = orderCache.query(cqry);
        for (Cache.Entry<String, OrderContext> e : cur) {
            System.out.println("query: " + e.getValue());
        }
    }
}
