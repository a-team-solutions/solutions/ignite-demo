package grid.client.flow;

import java.util.Arrays;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.SqlFieldsQuery;

import grid.Caches;
import grid.model.OrderContext;
import grid.server.Conf;

public class Status {

    public static void main(String[] args) throws Exception {
        // System.out.println("args: " + Arrays.toString(args));

        Ignition.setClientMode(true);
        Ignite ignite = Ignition.start(new Conf());

        // Activate the cluster. Required to do if the persistent store is enabled because you might need
        // to wait while all the nodes, that store a subset of data on disk, join the cluster.
        ignite.cluster().active(true);

        IgniteCache<String, OrderContext> orderCache = ignite.getOrCreateCache(Caches.OREDR);

        while (true) {
            SqlFieldsQuery qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext");
            System.out.println("stat: " + orderCache.query(qry).getAll().get(0).get(0));

            qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext" +
                "   where payDate is null and deliverDate is null");
            System.out.println("stat new: " + orderCache.query(qry).getAll().get(0).get(0));

            qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext" +
                "   where payDate is not null and deliverDate is null");
            System.out.println("stat payed: " + orderCache.query(qry).getAll().get(0).get(0));

            qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext" +
                "   where payDate is not null and deliverDate is not null");
            System.out.println("stat delivered: " + orderCache.query(qry).getAll().get(0).get(0));

            Thread.sleep(1_000);
        }
    }
}
