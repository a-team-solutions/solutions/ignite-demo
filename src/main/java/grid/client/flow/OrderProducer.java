package grid.client.flow;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteDataStreamer;
import org.apache.ignite.Ignition;

import grid.Caches;
import grid.model.OrderContext;
import grid.server.Conf;

public class OrderProducer {

    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start(new Conf())) {
            // Activate the cluster. Required to do if the persistent store is enabled because you might need
            // to wait while all the nodes, that store a subset of data on disk, join the cluster.
            ignite.cluster().active(true);

            IgniteCache<String, OrderContext> orderCache = ignite.getOrCreateCache(Caches.OREDR);

            Random random = new Random();

            try (IgniteDataStreamer<String, OrderContext> stmr = ignite.dataStreamer(orderCache.getName())) {
                stmr.autoFlushFrequency(1_000);
                int i = 0;
                while (true) {
                    double price = new BigDecimal(random.nextDouble() * 10)
                        .setScale(2, RoundingMode.HALF_UP)
                        .doubleValue();
                    OrderContext order = new OrderContext("order " + i++, price);
                    stmr.addData(order.getId(), order);
                    // stmr.tryFlush();
                    System.out.println("new " + i + " " + order);
                    Thread.sleep(100);
                }
            }
        }
    }
}
