package grid.client.flow;

import java.util.Date;

import javax.cache.Cache;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.lang.IgniteBiPredicate;
import org.apache.ignite.transactions.Transaction;

import grid.Caches;
import grid.model.OrderContext;
import grid.server.Conf;

public class DeliverCheck {

    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);
        Ignite ignite = Ignition.start(new Conf());

        // Activate the cluster. Required to do if the persistent store is enabled because you might need
        // to wait while all the nodes, that store a subset of data on disk, join the cluster.
        ignite.cluster().active(true);

        IgniteCache<String, OrderContext> orderCache = ignite.getOrCreateCache(Caches.OREDR);

        IgniteBiPredicate<String, OrderContext> filter = (k, v) ->
            v.getPayDate() != null &&
            v.getDeliverDate() == null &&
            ((new Date().getTime() - v.getPayDate().getTime()) > 15_000);

        IgniteTransactions transactions = ignite.transactions();

        while (true) {
            try (QueryCursor<Cache.Entry<String, OrderContext>> cursor = orderCache
                    .query(new ScanQuery<String, OrderContext>(filter))) {
                for (Cache.Entry<String, OrderContext> entry : cursor) {
                    OrderContext o = entry.getValue();
                    System.out.println("delivered " + (new Date().getTime() - o.getPayDate().getTime()) + entry.getValue());
                    orderFlagDelivered(orderCache, transactions, o);
                }
            }
            Thread.sleep(5_000);
        }
    }

    private static void orderFlagDelivered(
            IgniteCache<String, OrderContext> orderCache,
            IgniteTransactions transactions,
            OrderContext o) {
        try (Transaction tx = transactions.txStart()) {
            o.setDeliverDate(new Date());
            orderCache.put(o.getId(), o);
            tx.commit();
        }
    }
}
