package grid.client;

import java.util.List;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.events.EventType;

import grid.model.Person;

public class QuerySqlClient {

    public static void main(String[] args) throws IgniteException {
        Ignition.setClientMode(true);

        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setPeerClassLoadingEnabled(true);
        cfg.setIncludeEventTypes(EventType.EVTS_ALL);

        try (Ignite ignite = Ignition.start(cfg)) {

            IgniteCache<Long, Person> cache = ignite.getOrCreateCache(
                new CacheConfiguration<Long, Person>()
                    .setName(Person.class.getSimpleName())
                    .setIndexedTypes(Long.class, Person.class));

            SqlFieldsQuery qry = new SqlFieldsQuery(
                "SELECT count(*) from \"Person\".Person");
            long count = (long)cache.query(qry).getAll().get(0).get(0);
            System.out.println(count);

            qry = new SqlFieldsQuery(
                "insert into Person" +
                "   (_key, id, name, salary)" +
                "   values (?, ?, ?, ?)");
            cache.query(qry.setArgs(count + 1L, count + 1L, "John", 4000 + count + 1L));
            cache.query(qry.setArgs(count + 2L, count + 2L, "Jane", 2000 + count + 2L));
            cache.query(qry.setArgs(count + 3L, count + 3L, "Mary", 5000 + count + 3L));
            cache.query(qry.setArgs(count + 4L, count + 4L, "Richard", 3000 + count + 4L));
            cache.query(qry.setArgs(count + 5L, count + 5L, "Peter", 7000 + count + 5L));

            qry = new SqlFieldsQuery(
                "SELECT * from \"Person\".Person" +
                "   where salary > 3000" +
                "   order by salary desc"
                );

            // List<List<?>> all = cache.query(qry).getAll();
            try (QueryCursor<List<?>> cursor = cache.query(qry)) {
                for (List<?> row : cursor) {
                    System.out.println("row: " + row);
                }
            }
        }
    }
};
