package grid.client;

import java.util.Collection;

import org.apache.ignite.DataRegionMetrics;
import org.apache.ignite.DataStorageMetrics;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMetrics;

import grid.Caches;
import grid.model.OrderContext;
import grid.server.Conf;

public class MetricsClient {

    public static void main(String[] args) throws IgniteException {
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start(new Conf())) {
            // Activate the cluster. Required to do if the persistent store is enabled because you might need
            // to wait while all the nodes, that store a subset of data on disk, join the cluster.
            ignite.cluster().active(true);

            IgniteCache<String, OrderContext> orderCache = ignite.getOrCreateCache(Caches.OREDR);

            CacheMetrics cm = orderCache.metrics();
            System.out.println("Metrics cache:");
            System.out.println("\tAvg put time: " + cm.getAveragePutTime());
            System.out.println("\tAvg get time: " + cm.getAverageGetTime());

            Collection<DataRegionMetrics> regionsMetrics = ignite.dataRegionMetrics();
            System.out.println("Metrics data:");
            for (DataRegionMetrics rm : regionsMetrics) {
                System.out.println("\tMemory Region Name: " + rm.getName());
                System.out.println("\tAllocation Rate: " + rm.getAllocationRate());
                System.out.println("\tFill Factor: " + rm.getPagesFillFactor());
                System.out.println("\tAllocated Size: " + rm.getTotalAllocatedPages());
                System.out.println("\tPhysical Memory Size: " + rm.getPhysicalMemorySize());
            }

            DataStorageMetrics storageMetrics = ignite.dataStorageMetrics();
            System.out.println("Metrics storage:");
            System.out.println("\tFsync duration: " + storageMetrics.getLastCheckpointFsyncDuration());
            System.out.println("\tData pages: " + storageMetrics.getLastCheckpointDataPagesNumber());
            System.out.println("\tCheckpoint duration:" + storageMetrics.getLastCheckpointDuration());
        }
    }
}
