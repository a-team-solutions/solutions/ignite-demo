package grid.client;

import java.util.Collection;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.lang.IgniteCallable;
import org.apache.ignite.resources.IgniteInstanceResource;

import grid.model.OrderContext;

public class ComputeClient {

    public static void main(String[] args) throws IgniteException {
        Ignition.setClientMode(true);

        IgniteConfiguration cfg = new IgniteConfiguration();

        // Ignite ignite = Ignition.start(cfg);
        try (Ignite ignite = Ignition.start(cfg)) {
            IgniteCompute compute = ignite.compute();

            compute.broadcast(() -> System.out.println("Compute Hello Server"));

            compute.callAsync(() -> "Compute Hello Server Async")
                .listen(f -> System.out.println("Job result: " + f.get()));

            Collection<String> res = compute.broadcast(new IgniteCallable<String>() {
                private static final long serialVersionUID = 1L;

                @IgniteInstanceResource
                private Ignite ignite;

                @Override
                public String call() throws Exception {
                    String orderCacheName = OrderContext.class.getSimpleName();
                    IgniteCache<String, OrderContext> cache = ignite.getOrCreateCache(orderCacheName);
                    // Do some stuff with cache.
                    // ...
                    return ignite.name() + cache.getName();
                }
            });
            System.out.println("Compute response: " + res);
        }
    }
}
