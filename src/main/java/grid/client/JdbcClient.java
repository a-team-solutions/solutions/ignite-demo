package grid.client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcClient {

    public static void main(String[] args) throws Exception {
        // Open the JDBC connection via DriverManager.
        Class.forName("org.apache.ignite.IgniteJdbcDriver");

        // Or open connection via DataSource.
        // IgniteJdbcThinDataSource dataSource = new IgniteJdbcThinDataSource();
        // dataSource.setUrl("jdbc:ignite:thin://localhost");
        // dataSource.setDistributedJoins(true);

        // try (Connection conn = dataSource.getConnection()) {

        try (Connection conn = DriverManager.getConnection("jdbc:ignite:thin://localhost")) {

            try (Statement stmt = conn.createStatement()) {
                // Create table based on REPLICATED template
                stmt.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS City (" +
                    "   id LONG PRIMARY KEY, name VARCHAR) " +
                    "   WITH \"template=replicated\"");

                // Create table based on PARTITIONED template with one backup
                stmt.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS Person (" +
                    "   id LONG, name VARCHAR, city_id LONG, " +
                    "   PRIMARY KEY (id, city_id)) " +
                    "   WITH \"backups=1, affinityKey=city_id\"");
            }

            try (Statement stmt = conn.createStatement()) {
                // Create an index on the City table
                stmt.executeUpdate(
                    "CREATE INDEX IF NOT EXISTS idx_city_name ON City (name)");

                // Create an index on the Person table
                stmt.executeUpdate(
                    "CREATE INDEX IF NOT EXISTS idx_person_name ON Person (name)");
            }

            int count = 0;

            try (Statement stmt = conn.createStatement()) {
                try (ResultSet rs = stmt.executeQuery("SELECT count(*) as count FROM Person")) {
                    System.out.println(rs);
                    rs.next();
                    count = rs.getInt("count");
                    System.out.println(count);
                }
            }


            if (count == 0) {
                // Populate City table
                try (PreparedStatement stmt = conn.prepareStatement(
                    "INSERT INTO City (id, name) VALUES (?, ?)")) {

                    stmt.setLong(1, 1L);
                    stmt.setString(2, "Forest Hill");
                    stmt.executeUpdate();

                    stmt.setLong(1, 2L);
                    stmt.setString(2, "Denver");
                    stmt.executeUpdate();

                    stmt.setLong(1, 3L);
                    stmt.setString(2, "St. Petersburg");
                    stmt.executeUpdate();
                }

                // Populate Person table
                try (PreparedStatement stmt = conn.prepareStatement(
                    "INSERT INTO Person (id, name, city_id) VALUES (?, ?, ?)")) {

                    stmt.setLong(1, 1L);
                    stmt.setString(2, "John Doe");
                    stmt.setLong(3, 3L);
                    stmt.executeUpdate();

                    stmt.setLong(1, 2L);
                    stmt.setString(2, "Jane Roe");
                    stmt.setLong(3, 2L);
                    stmt.executeUpdate();

                    stmt.setLong(1, 3L);
                    stmt.setString(2, "Mary Major");
                    stmt.setLong(3, 1L);
                    stmt.executeUpdate();

                    stmt.setLong(1, 4L);
                    stmt.setString(2, "Richard Miles");
                    stmt.setLong(3, 2L);
                    stmt.executeUpdate();
                }
            }

            // Get data using an SQL join sample.
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(
                    "SELECT p.name, c.name " +
                    "   FROM Person p, City c " +
                    "   WHERE p.city_id = c.id")) {
                    System.out.println("Query result:");
                    while (rs.next()) {
                        System.out.println(">>> " + rs.getString(1) + ", " + rs.getString(2));
                    }
                }
            }

            // ResultSet rs = conn.createStatement()
            //     .executeQuery("select * from " + orderCacheName + "");

            // while (rs.next()) {
            //     String name = rs.getString(1);
            //     System.out.println(name);
            // }

            // // Query people with specific age using prepared statement.
            // PreparedStatement stmt = conn.prepareStatement("select name, age from Person
            // where age = ?");

            // stmt.setInt(1, 30);

            // ResultSet rs = stmt.executeQuery();

            // while (rs.next()) {
            // String name = rs.getString("name");
            // int age = rs.getInt("age");
            // // ...
            // // }
            // }
        }
    }
}
