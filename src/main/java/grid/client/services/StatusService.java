package grid.client.services;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;

import grid.Caches;
import grid.model.OrderContext;

public class StatusService implements Service {

    private static final long serialVersionUID = 1L;

    @IgniteInstanceResource
    private Ignite ignite;

    private IgniteCache<String, OrderContext> orderCache;

    private String serviceName;

    @Override
    public void init(ServiceContext ctx) throws Exception {
        orderCache = ignite.getOrCreateCache(Caches.OREDR);
        serviceName = ctx.name();
        System.out.println("Service init: " + serviceName);
    }

    @Override
    public void cancel(ServiceContext ctx) {
        System.out.println("Service cancel: " + serviceName);
    }

    @Override
    public void execute(ServiceContext ctx) throws Exception {
        System.out.println("Service execute: " + serviceName);
        while (!ctx.isCancelled()) {
            SqlFieldsQuery qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext");
            System.out.println("stat: " + orderCache.query(qry).getAll().get(0).get(0));

            qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext" +
                "   where payDate is null and deliverDate is null");
            System.out.println("stat new: " + orderCache.query(qry).getAll().get(0).get(0));

            qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext" +
                "   where payDate is not null and deliverDate is null");
            System.out.println("stat payed: " + orderCache.query(qry).getAll().get(0).get(0));

            qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext" +
                "   where payDate is not null and deliverDate is not null");
            System.out.println("stat delivered: " + orderCache.query(qry).getAll().get(0).get(0));

            Thread.sleep(1_000);
        }
    }

}
