package grid.client.services;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteServices;
import org.apache.ignite.Ignition;

import grid.server.Conf;

public class StatusServiceUndeploy {

    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);
        try (Ignite ignite = Ignition.start(new Conf())) {
            IgniteServices services = ignite.services();

            services.cancel(StatusService.class.getSimpleName());

            services.serviceDescriptors()
                .forEach(desc -> System.out.println(desc));
        }
    }
}