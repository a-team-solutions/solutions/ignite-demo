package grid.client.services;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteServices;
import org.apache.ignite.Ignition;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.services.ServiceConfiguration;

import grid.Caches;
import grid.server.Conf;

public class StatusServiceDeploy {

    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start(new Conf())) {

            // Cluster Group Based Deployment

            ClusterGroup cacheGroup = ignite.cluster()
                .forCacheNodes(Caches.OREDR.getName());
                // .forServers();

            IgniteServices services = ignite.services(cacheGroup);

            services.deployClusterSingleton(
                StatusService.class.getSimpleName(),
                new StatusService());

            // Node Filter Based Deployment

            // ServiceConfiguration scfg = new ServiceConfiguration();
            // scfg.setName(StatusService.class.getSimpleName());
            // scfg.setService(new StatusService());
            // scfg.setMaxPerNodeCount(1);
            // // scfg.setCacheName(Caches.OREDR.getName());
            // // scfg.setNodeFilter(new ServiceFilter());

            // IgniteServices services = ignite.services();
            // services.deploy(scfg);

            services.serviceDescriptors()
                .forEach(desc -> System.out.println(desc.name()));
        }
    }
}