package grid.client.services.openapi;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteServices;
import org.apache.ignite.Ignition;

import grid.server.Conf;

public class ApiServiceDeploy {

    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start(new Conf())) {
            IgniteServices services = ignite.services();

            services.deployNodeSingleton(
                ApiService.class.getSimpleName(),
                new ApiService());

            services.serviceDescriptors()
                .forEach(desc -> System.out.println(desc.name()));
        }
    }
}