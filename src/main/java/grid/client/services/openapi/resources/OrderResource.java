package grid.client.services.openapi.resources;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import grid.client.services.openapi.logic.Logic;

@Path("/order")
public class OrderResource {

    @Context
    ServletConfig config;

    @Context
    Application app;

    @Inject
    Logic logic;

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String test() {
        Long count = logic.count();
        System.out.println("Order count: " + count);
        return "Order count " + count;
    }
}
