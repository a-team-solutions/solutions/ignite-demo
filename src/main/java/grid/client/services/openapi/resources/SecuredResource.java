package grid.client.services.openapi.resources;

import java.security.Key;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import grid.client.services.openapi.filter.AuthRequestFilter;
import grid.client.services.openapi.filter.AuthRoles;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Path("/secured")
public class SecuredResource {

    public static String secret = AuthRequestFilter.secret;
    public static Key key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({ AuthRoles.ADMIN, AuthRoles.USER })
    public String get(@Context SecurityContext securityContext) {
        return "Username: " + securityContext.getUserPrincipal().getName();
    }

    @GET
    @Path("/jwt")
    @Produces(MediaType.APPLICATION_JSON)
    public String jwt(@Context SecurityContext securityContext) {
        String jws = Jwts.builder()
            // .setIssuer("ignite")
            .setSubject("peter")
            // .setAudience("you")
            // .setExpiration(expiration)
            // .setNotBefore(notBefore)
            // .setIssuedAt(new Date())
            // .setId(UUID.randomUUID().toString())
            .claim("role", AuthRoles.ADMIN)
            .signWith(key)
            .compact();
        System.out.println("jwt: " + jws);

        try {
            Jws<Claims> claims = Jwts.parserBuilder()
                .setSigningKey(AuthRequestFilter.key)
                .build()
                .parseClaimsJws(jws);
            System.out.println("claims: " + claims);
        } catch (JwtException e) {
            System.out.println("error: " + e);
        }

        return "curl -H 'Accept: application/json' -H 'Authorization: Bearer " + jws + "' localhost:8090/secured";
    }

}
