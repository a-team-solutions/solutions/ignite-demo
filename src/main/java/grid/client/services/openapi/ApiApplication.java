package grid.client.services.openapi;

import java.net.URI;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletConfig;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriBuilder;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.ServletProperties;

import grid.client.services.openapi.logic.Logic;
import grid.client.services.openapi.logic.MemoryLogic;
import grid.client.services.openapi.filter.CorsResponseFilter;
import grid.client.services.openapi.filter.AuthRequestFilter;
import io.swagger.v3.jaxrs2.SwaggerSerializers;
import io.swagger.v3.jaxrs2.integration.JaxrsOpenApiContextBuilder;
import io.swagger.v3.oas.integration.OpenApiConfigurationException;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

// @ApplicationPath("/api")
public class ApiApplication extends ResourceConfig {

    public static Logic logic = new MemoryLogic();

    @Context
    ServletConfig servletConfig;

    private String resourcePackages = getClass().getPackage().getName() + ".resources";

    public ApiApplication() {
        super();
        init();
    }

    public ApiApplication(Logic logic) {
        super();
        ApiApplication.logic = logic;
        init();
    }

    public void init() {
        initOpenApi();

        packages(true, resourcePackages);
        // registerClasses(resourceClasses());

        // Use @Inject to bind the class instance
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(logic).to(Logic.class);
                // bind(MemoryLogic.class).to(Logic.class).in(Singleton.class);
                // bind(MemoryLogic.class).to(Logic.class).in(PerLookup.class);
            }
        });

        // register(BasicAuthRequestFilter.class);
        register(AuthRequestFilter.class);

        register(CorsResponseFilter.class);

        // User roles and authentication
        // register(RolesAllowedDynamicFeature.class);
        // register(UserAuthenticationFilter.class);

        // OpenAPI documentation
        // register(ApiListingResource.class);
        // register(SwaggerSerializers.class);
    }

    private void initOpenApi() {
        OpenAPI oas = new OpenAPI();
        Info info = new Info()
            .title("Swagger Sample App bootstrap code")
            .description("This is a sample server Petstore server.  You can find out more about Swagger " +
                "at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, " +
                "you can use the api key `special-key` to test the authorization filters.")
            .termsOfService("http://swagger.io/terms/")
            .contact(new Contact()
                .email("apiteam@swagger.io"))
            .license(new License()
                .name("Apache 2.0")
                .url("http://www.apache.org/licenses/LICENSE-2.0.html"));
        oas.info(info);
        SwaggerConfiguration oasConfig = new SwaggerConfiguration()
            .openAPI(oas)
            .prettyPrint(true)
            .resourcePackages(Stream.of(resourcePackages).collect(Collectors.toSet()));
            // .resourceClasses(
            //     resourceClasses()
            //         .stream()
            //         .map(c -> c.getCanonicalName())
            //         .collect(Collectors.toSet()));
        try {
            new JaxrsOpenApiContextBuilder<>()
                .servletConfig(servletConfig)
                .application(this)
                .openApiConfiguration(oasConfig)
                .buildContext(true);
        } catch (OpenApiConfigurationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        packages(true, "io.swagger.v3.jaxrs2.integration.resources");
    }

    // public static void initServletContextHandler(Server server) {
    //     ServletContextHandler sch = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
    //     sch.setContextPath("/");
    //     server.setHandler(sch);

    //     ServletHolder sh = sch.addServlet(ServletContainer.class, "/api/*");
    //     sh.setInitOrder(0);
    //     sh.setInitParameter(
    //         ServletProperties.JAXRS_APPLICATION_CLASS,
    //         ApiApplication.class.getCanonicalName());
    //     // servletHolder.setInitParameter(
    //     //    "jersey.config.server.provider.classnames",
    //     //    TestResource.class.getCanonicalName() + " " +
    //     //    HelloResource.class.getCanonicalName());
    //     // servletHolder.setInitParameter(
    //     //     "jersey.config.server.provider.packages",
    //     //     // "grid.client.services.resources");
    //     //     String.join(",",
    //     //         "grid.client.services.resources",
    //     //         "io.swagger.v3.jaxrs2.integration.resources"));

    //     // ServletHolder openapiHolder = sch.addServlet(OpenApiServlet.class, "/openapi/*");
    //     // openapiHolder.setInitOrder(1);
    //     // openapiHolder.setInitParameter(
    //     //     "openApi.configuration.resourcePackages",
    //     //     "grid.client.services.resources");
    // }

    public static Server createServer(int port) {
        // Server server = new Server(port);
        // // server.setHandler(new StatusHandler(orderCache));
        // ApiApplication.initServletContextHandler(server);

        URI baseUri = UriBuilder.fromUri("http://localhost/").port(8090).build();
        ResourceConfig config = new ApiApplication();
        Server server = JettyHttpContainerFactory.createServer(baseUri, config);

        return server;
    }

    public static void main(String[] args) throws Exception {
        ApiApplication.logic = new MemoryLogic();
        Server server = createServer(8090);
        try {
            server.start();
            server.join();
        } catch (Exception ex) {
            System.out.println("Error start Jetty: " + ex);
            System.exit(1);
        } finally {
            server.destroy();
        }
    }

}

// class StatusHandler extends AbstractHandler {

//     private IgniteCache<String, OrderContext> orderCache;

//     StatusHandler(IgniteCache<String, OrderContext> orderCache) {
//         this.orderCache = orderCache;
//     }

//     public void handle(String target,
//                        Request baseRequest,
//                        HttpServletRequest request,
//                        HttpServletResponse response)
//         throws IOException, ServletException
//     {
//         Long count = 0L;

//         if (orderCache != null) {
//             SqlFieldsQuery qry = new SqlFieldsQuery(
//                 "select count(*)" +
//                 "   from \"OrderContext\".OrderContext");
//             count = (Long)orderCache.query(qry).getAll().get(0).get(0);
//             System.out.println("count: " + count);
//         }

//         response.setContentType("text/html;charset=utf-8");
//         response.setStatus(HttpServletResponse.SC_OK);
//         baseRequest.setHandled(true);
//         response.getWriter().println("<h1>count: " + count + "</h1>");
//     }
// }
