package grid.client.services.openapi;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.apache.ignite.services.Service;
import org.apache.ignite.services.ServiceContext;

import grid.Caches;
import grid.client.services.openapi.logic.IgniteLogic;
import grid.model.OrderContext;


import org.eclipse.jetty.server.Server;

public class ApiService implements Service {

    private static final long serialVersionUID = 1L;

    @IgniteInstanceResource
    private Ignite ignite;

    private IgniteCache<String, OrderContext> orderCache;

    private String serviceName;

    private Server server;

    @Override
    public void init(ServiceContext ctx) throws Exception {
        serviceName = ctx.name();
        System.out.println("Service init: " + serviceName);

        orderCache = ignite.getOrCreateCache(Caches.OREDR);

        ApiApplication.logic = new IgniteLogic(ignite);
        server = ApiApplication.createServer(8090);
    }

    @Override
    public void cancel(ServiceContext ctx) {
        System.out.println("Service cancel: " + serviceName);
        try {
            server.stop();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(ServiceContext ctx) throws Exception {
        System.out.println("Service execute: " + serviceName);
        server.start();
    }
}
