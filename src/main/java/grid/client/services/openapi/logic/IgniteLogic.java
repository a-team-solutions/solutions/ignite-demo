package grid.client.services.openapi.logic;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cache.query.SqlFieldsQuery;

import grid.Caches;
import grid.model.OrderContext;

public class IgniteLogic implements Logic {

    public static Ignite ignite;

    public IgniteLogic(Ignite ignite) {
        IgniteLogic.ignite = ignite;
    }

    public Long count() {
        IgniteCache<String, OrderContext> orderCache = ignite.getOrCreateCache(Caches.OREDR);
        SqlFieldsQuery qry = new SqlFieldsQuery(
                "select count(*)" +
                "   from \"OrderContext\".OrderContext");
        Long count = (Long)orderCache.query(qry).getAll().get(0).get(0);
        return count;
    };
}