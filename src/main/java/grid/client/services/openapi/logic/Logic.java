package grid.client.services.openapi.logic;

public interface Logic {

    Long count();
}
