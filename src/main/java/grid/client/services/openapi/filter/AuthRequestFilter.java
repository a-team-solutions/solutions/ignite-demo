package grid.client.services.openapi.filter;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.security.KeyPair;
import java.security.Principal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.UUID;

import org.glassfish.jersey.internal.util.Base64;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthRequestFilter implements ContainerRequestFilter {

    static Map<String, User> users = new HashMap<>();

    static {
        User user = new User("test", "test", AuthRoles.ADMIN);
        users.put(user.username, user);
    }

    // public static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    public static String secret = "C1wXpYwj6nz3tDMT3cnMbj69Pix0SHsPaeRn1qja7F0=";
    public static Key key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Method method = resourceInfo.getResourceMethod();

        // Access allowed for all
        if (method.isAnnotationPresent(PermitAll.class)) {
            return;
        }

        // Access denied for all
        if (method.isAnnotationPresent(DenyAll.class)) {
            requestContext.abortWith(forbiddenResponse());
            return;
        }

        if (method.isAnnotationPresent(RolesAllowed.class)) {

            RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
            Set<String> roles = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

            String authHeader = requestContext.getHeaderString("Authorization");

            if (authHeader == null) {
                requestContext.abortWith(unauthorizedResponse());
                return;
            }

            if (authHeader.startsWith("Bearer ")) {
                String token = authHeader.split(" ")[1];
                authJwt(token, requestContext, roles);
            } else if (authHeader.startsWith("Basic ")) {
                String token = authHeader.split(" ")[1];
                authBasic(token, requestContext, roles);
            } else {
                requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED).build());
            }
        }
    }

    private void authBasic(String token, ContainerRequestContext requestContext, Set<String> roles) {
        String usernameAndPassword = new String(Base64.decode(token.getBytes()));;
        StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
        if (tokenizer.countTokens() == 2) {
            String username = tokenizer.nextToken();
            String password = tokenizer.nextToken();

            System.out.println(
                "Auth username=" + username +
                " password=" + password +
                " roles=" + roles);

            User user = AuthRequestFilter.users.get(username);
            boolean authorized = user != null &&
                user.username.equals(username) &&
                user.password.equals(password) &&
                roles.contains(user.role);

            if (!authorized) {
                requestContext.abortWith(unauthorizedResponse());
                return;
            } else {
                SecurityContext sc = requestContext.getSecurityContext();
                requestContext.setSecurityContext(
                    new AuthSecurityContext(
                        username,
                        users.get(username).role,
                        sc));
            }
        } else {
            requestContext.abortWith(unauthorizedResponse());
            return;
        }
    }

    private void authJwt(String token, ContainerRequestContext requestContext, Set<String> roles) {
        try {
            Jws<Claims> claims = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);
            System.out.println("claims: " + claims);

            if (!roles.contains(claims.getBody().get("role"))) {
                requestContext.abortWith(unauthorizedResponse());
                return;
            }

            SecurityContext sc = requestContext.getSecurityContext();
            requestContext.setSecurityContext(
                new AuthSecurityContext(
                    claims.getBody().getSubject().toString(),
                    claims.getBody().get("role").toString(),
                    sc));
        } catch (JwtException e) {
            requestContext.abortWith(
                Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity(e.getMessage())
                    .build());
        }
    }

    private Response forbiddenResponse() {
        return Response
            .status(Response.Status.FORBIDDEN)
            .entity("Access denied for all")
            .build();
    }

    private Response unauthorizedResponse() {
        return Response
            .status(Response.Status.UNAUTHORIZED)
            .header("WWW-Authenticate", "Basic")
            .entity("Access denied, unauthorized")
            .build();
    }

    public static void main(String[] args) {

        // We need a signing key, so we'll create one just for this example. Usually
        // the key would be read from your application configuration instead.
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        String keyString = Encoders.BASE64.encode(key.getEncoded());
        System.out.println("key: " + keyString);

        Key skey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(keyString));

        KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256);
        String keyPubString = Encoders.BASE64.encode(keyPair.getPublic().getEncoded());
        String keyPrivString = Encoders.BASE64.encode(keyPair.getPrivate().getEncoded());
        System.out.println("keyPub: " + keyPubString);
        System.out.println("keyPriv: " + keyPrivString);

        String jws = Jwts.builder()
            .setIssuer("me")
            .setSubject("Peter")
            .setAudience("you")
            // .setExpiration(expiration)
            // .setNotBefore(notBefore)
            .setIssuedAt(new Date())
            .setId(UUID.randomUUID().toString())
            .claim("role", "admin")
            // .signWith(key)
            .signWith(skey)
            // .signWith(keyPair.getPrivate())
            .compact();
        System.out.println("jwt: " + jws);

        boolean ok = Jwts.parserBuilder()
            // .setSigningKey(key)
            .setSigningKey(skey)
            // .setSigningKey(keyPair.getPublic())
            .build()
            .parseClaimsJws(jws)
            .getBody()
            .getSubject()
            .equals("Peter");
        System.out.println("verify: " + ok);

        try {
            Jws<Claims> claims = Jwts.parserBuilder()
                // .setSigningKey(key)
                .setSigningKey(skey)
                // .setSigningKey(keyPair.getPublic())
                .build()
                .parseClaimsJws(jws);
                System.out.println("claims: " + claims);
            // OK, we can trust this JWT
        } catch (JwtException e) {
            // don't trust the JWT!
            System.out.println("error: " + e);
        }
    }
}

class AuthSecurityContext implements SecurityContext {

    private String name;
    private String role;
    private SecurityContext sc;

    public AuthSecurityContext(String name, String role, SecurityContext sc) {
        this.name = name;
        this.role = role;
        this.sc = sc;
    }

    @Override
    public Principal getUserPrincipal() {
        return new Principal() {
            @Override
            public String getName() {
                return name;
            }
        };
    }

    @Override
    public boolean isUserInRole(String role) {
        return this.role.equals(role);
    }

    @Override
    public boolean isSecure() {
        return sc.isSecure();
    }

    @Override
    public String getAuthenticationScheme() {
        return sc.getAuthenticationScheme();
    }
}

class User {

    public String username;
    public String password;
    public String role;

    User(String username, String password, String role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() +
            " username=" + username +
            " password=" + password +
            " role=" + role ;
    }
}
