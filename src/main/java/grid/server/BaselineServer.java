package grid.server;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteException;
import org.apache.ignite.Ignition;

public class BaselineServer {

    public static void main(String[] args) throws IgniteException {

        // System.setProperty("java.util.logging.config.file", "logging.properties");

        Ignite ignite = Ignition.start(new Conf());

        // Activate the cluster. Required to do if the persistent store is enabled because you might need
        // to wait while all the nodes, that store a subset of data on disk, join the cluster.
        ignite.cluster().active(true);

        ignite.log().info("caches: " + ignite.cacheNames());

        System.out.println("caches: " + ignite.cacheNames());
    }
}
