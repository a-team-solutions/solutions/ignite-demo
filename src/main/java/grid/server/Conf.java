package grid.server;

import java.util.Arrays;

import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
// import org.apache.ignite.spi.deployment.uri.UriDeploymentSpi;

public class Conf extends IgniteConfiguration {

    public Conf() {
        // TcpCommunicationSpi commSpi = new TcpCommunicationSpi();
        // commSpi.setSlowClientQueueLimit(1000);

        DataStorageConfiguration dsCfg = new DataStorageConfiguration();
        dsCfg.setMetricsEnabled(true);
        // dsCfg.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
        // dsCfg.setStoragePath("/opt/storage");

        // String orderCacheName = Order.class.getSimpleName();
        // CacheConfiguration<String, Order> cacheCfg = new CacheConfiguration<>(orderCacheName);
        // cacheCfg.setCacheMode(CacheMode.PARTITIONED);
        // cacheCfg.setBackups(1);
        // cacheCfg.setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC);
        // cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);

        setPeerClassLoadingEnabled(true);
        // setCommunicationSpi(commSpi);
        setDataStorageConfiguration(dsCfg);
        // setCacheConfiguration(cacheCfg);
        // setIncludeEventTypes(EventType.EVTS_ALL);

        // IgniteLogger log = new Slf4jLogger();
        // setGridLogger(log);

        // https://apacheignite.readme.io/docs/rest-api#security
        // setAuthenticationEnabled(true);

        // UriDeploymentSpi deploymentSpi = new UriDeploymentSpi();
        // deploymentSpi.setUriList(Arrays.asList("file:///home/peter/projects-ng/A-Team/solutions/ignite-demo/target/classes"));
        // setDeploymentSpi(deploymentSpi);
    }
}
