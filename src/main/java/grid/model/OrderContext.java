package grid.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.apache.ignite.cache.query.annotations.QueryTextField;

// import org.apache.ignite.cache.query.annotations.QuerySqlField;
// import org.apache.ignite.cache.query.annotations.QueryTextField;

public class OrderContext implements Serializable {

    private static final long serialVersionUID = 1L;

    @QuerySqlField(index = true)
    private String id;

    @QuerySqlField
    private Date submitDate;

    @QueryTextField
    private String title;

    @QuerySqlField(index = true)
    private Double price;

    @QuerySqlField(index = true)
    private Date payDate;

    @QuerySqlField(index = true)
    private Date deliverDate;

    public OrderContext(String title, Double price) {
        this.title = title;
        this.price = price;
        this.submitDate = new Date();
    }

    public String getId() {
        if (id == null) {
            id = UUID.randomUUID().toString();
        }
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date date) {
        this.submitDate = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public Date getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(Date deliverDate) {
        this.deliverDate = deliverDate;
    }

    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName()
            + " id=" + id
            + " submit=" + submitDate
            + " title=" + title
            + " price=" + price
            + " pay=" + payDate
            + " deliver=" + deliverDate
            + "]";
    }
}
