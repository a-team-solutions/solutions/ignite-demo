package grid;

import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.configuration.CacheConfiguration;

import grid.model.OrderContext;

public class Caches {

    public static final CacheConfiguration<String, OrderContext> OREDR =
        new CacheConfiguration<String, OrderContext>()
            .setName(OrderContext.class.getSimpleName())
            .setStatisticsEnabled(true)
            .setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL)
            .setBackups(1)
            .setWriteSynchronizationMode(CacheWriteSynchronizationMode.FULL_SYNC)
            .setIndexedTypes(String.class, OrderContext.class);

}
