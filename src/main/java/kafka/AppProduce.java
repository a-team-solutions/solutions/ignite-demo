package kafka;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.common.serialization.LongSerializer;
// import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class AppProduce {

    public static void main(String[] args) {
        System.out.println("Produce");

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaConf.KAFKA_BROKERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, KafkaConf.CLIENT_ID);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        // props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CustomSerializer.class.getName());
        props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomPartitioner.class.getName());

        try (Producer<Long, CustomObject> producer = new KafkaProducer<>(props)) {
            for (int i = 0; i < KafkaConf.MESSAGE_COUNT; i++) {
                // ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(
                //     KafkaConf.TOPIC_NAME,
                //     Long.valueOf(i),
                //     "This is record " + i);
                ProducerRecord<Long, CustomObject> record = new ProducerRecord<Long, CustomObject>(
                    KafkaConf.TOPIC_NAME,
                    Long.valueOf(i),
                    new CustomObject("" + i, "name-" + i));
                try {
                    System.out.println("Producer send " + record);
                    Future<RecordMetadata> future = producer.send(record);
                    RecordMetadata metadata = future.get();
                    System.out.println("Record sent with key " + i + " to partition " + metadata.partition()
                            + " with offset " + metadata.offset());
                } catch (ExecutionException e) {
                    System.out.println("Error in sending record");
                    System.out.println(e);
                } catch (InterruptedException e) {
                    System.out.println("Error in sending record");
                    System.out.println(e);
                }
            }
        }
    }

}
