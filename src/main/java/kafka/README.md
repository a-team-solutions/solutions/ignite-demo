# Kafka setup

Based on

<https://dzone.com/articles/kafka-producer-and-consumer-example>
<https://hub.docker.com/r/bitnami/kafka>
<https://github.com/bitnami/bitnami-docker-kafka/blob/master/docker-compose.yml>

Start kafka container

```sh
docker-compose up
```

Setup topic partitions

```sh
docker-compose run kafka bash
kafka-topics.sh --zookeeper zookeeper:2181 --create --replication-factor 1 --partitions 100 --topic demo
kafka-topics.sh --zookeeper zookeeper:2181 --list
kafka-topics.sh --zookeeper zookeeper:2181 --topic demo --describe
```
