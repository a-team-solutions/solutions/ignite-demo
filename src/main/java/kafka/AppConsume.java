package kafka;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.common.serialization.LongDeserializer;
// import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class AppConsume {

    public static void main(String[] args) {
        System.out.println("Consume");

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaConf.KAFKA_BROKERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, KafkaConf.GROUP_ID_CONFIG);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        // props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomDeserializer.class.getName());
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, KafkaConf.MAX_POLL_RECORDS);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, KafkaConf.OFFSET_RESET_EARLIER);

        try (Consumer<Long, CustomObject> consumer = new KafkaConsumer<>(props);) {
            consumer.subscribe(Collections.singletonList(KafkaConf.TOPIC_NAME));
            int noMessageFound = 0;
            while (true) {
                ConsumerRecords<Long, CustomObject> records = consumer.poll(Duration.ofSeconds(1));
                if (records.count() == 0) {
                    noMessageFound++;
                    if (noMessageFound > KafkaConf.MAX_NO_MESSAGE_FOUND_COUNT)
                        // If no message found count is reached to threshold exit loop.
                        break;
                    else
                        continue;
                }
                // print each record.
                records.forEach(record -> {
                    System.out.println("Record Key " + record.key());
                    System.out.println("Record value " + record.value());
                    System.out.println("Record partition " + record.partition());
                    System.out.println("Record offset " + record.offset());
                });
                // commits the offset of record to broker.
                consumer.commitAsync();
            }
        }
    }

}
