import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // System.out.println("args: " + Arrays.toString(args));
        if (args.length > 0) {
            try {
                Class<?> cls = Class.forName(args[0]);
                Method method = cls.getMethod("main", String[].class);
                String[] params = argsShift(args);
                method.invoke(null, (Object) params); // static method doesn't have an instance
            } catch (Exception e) {
                System.out.println("error: " + e);
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("java -jar jarfile.jar classname [args]");
        }
    }

    private static String[] argsShift(String[] args) {
        List<String> argsList = new ArrayList<String>(Arrays.asList(args));
        argsList.remove(0);
        String[] params = {};
        params = argsList.toArray(params);
        return params;
    }
}