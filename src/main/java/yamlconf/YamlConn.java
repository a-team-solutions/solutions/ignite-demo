package yamlconf;

public class YamlConn {
    private String url;
    private int poolSize;

    YamlConn() {
    }

    YamlConn(String url, int poolSize) {
        this.url = url;
        this.poolSize = poolSize;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    @Override
    public String toString() {
        return String.format( "url: '%s' poolSize: %d", getUrl(), getPoolSize() );
    }
}
