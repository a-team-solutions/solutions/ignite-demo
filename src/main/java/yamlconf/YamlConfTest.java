package yamlconf;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonMerge;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class YamlConfTest {

    public static void main(String[] args) throws IOException {
        // default conf
        YamlConf conf = new YamlConf();
        conf.setVersion("version");
        conf.setReleased(new Date());
        conf.setConnection(new YamlConn("", 3));

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        ObjectReader objectReader = mapper.readerForUpdating(conf);
        objectReader.readValue(new File("src/main/java/yamlconf/conf.yaml"));
        objectReader.readValue(new File("src/main/java/yamlconf/conf-1.yaml"));

        System.out.println(conf);
    }
}
