package soap.server;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
// @SOAPBinding(style = Style.RPC)
@SOAPBinding(style = Style.DOCUMENT)
public interface Greeting {

    @WebMethod
    String helloWorld();

    @WebMethod
    String hello(String name);

}
