package soap.server;

import javax.jws.WebService;

@WebService(endpointInterface = "soap.server.Greeting")
public class GreetingImpl implements Greeting {

    @Override
    public String helloWorld() {
        return "Hello World";
    }

    @Override
    public String hello(String name) {
        return "Hello " + name;
    }

}
