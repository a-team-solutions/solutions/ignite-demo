package soap.server;

import javax.xml.ws.Endpoint;

// Endpoint publisher
public class GreetingServer {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/ws/greeting", new GreetingImpl());
    }

}
