package soap.client;

import generated.soap.Greeting;
import generated.soap.GreetingImplService;

public class GreetingClient {

    public static void main(String[] args) throws Exception {

        GreetingImplService service = new GreetingImplService();
        System.out.println(service.getServiceName());

        Greeting greeting = service.getGreetingImplPort();

        System.out.println(greeting.helloWorld());
        System.out.println(greeting.hello("Petko"));

    }

}
