# High performance data processing Ignite demo

## Goals

- Cross-platform system integrations
- Easy to manage data flows
- Minimaze data processing tech stack
- Accelerate lagacy DB based systems
- Reliable and smooth deployment
